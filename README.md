# Simple Factory Method [![build status](https://gitlab.com/cristiangary/simple-factory-method/badges/master/build.svg)](https://gitlab.com/cristiangary/simple-factory-method/commits/master)

# simple-factory-method

A really (simple factory) (factory method)  pattern for Spring project,  (a simple replacement for ServiceLocatorFactoryBean used in Spring)


# Gradle Groovy DSL
```gradle
implementation 'com.geektastes:simple-factory-method:1.1.0'
```
# Gradle Kotlin DSL
```gradle
compile("com.geektastes:simple-factory-method:1.1.0")
```

# Basics Factory method

Usage typically looks like this. 


### Java + springboot 

```java
public interface Phone {
    void call();
}


@SimpleFactoryComponent(factory = PhoneFactory.class, key = "IOS")
@Component
public class IosPhone implements Phone {

    @Override
    public void call() {
        //....
    }
}

@SimpleFactoryComponent(factory = PhoneFactory.class, key = "ANDROID")
@Component
public class AndroidPhone implements Phone {

    @Override
    public void call() {
        //....
    }
}


@SimpleFactoryMethod
public interface PhoneFactory {
    Phone get(String brand);
}


@EnableSimpleFactoryMethod
@SpringBootApplication
public class ExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExampleApplication.class, args);
    }
}

@Service
public class PhoneDialer{
    @Autowired
    private PhoneFactory phoneFactory;
    
    public void call(String brand){
        Phone phone = phoneFactory.get(brand);
        phone.call();
    }
}


```

### Kotlin + springboot 

```kotlin
interface Phone{
    fun call()
}


@SimpleFactoryComponent(factory = PhoneFactory::class, key = "IOS")
@Component
class IosPhone : Phone{
        override fun call() {
          // ....
        }
}

@SimpleFactoryComponent(factory = PhoneFactory::class, key = "Android")
@Component
class AndroidPhone : Phone{
        override fun call() {
          // ....
        }
}

@SimpleFactoryComponent(factory = PhoneFactory::class, key = NokiaPhone.KEY)
@Component
class NokiaPhone : Phone {
    companion object {
        const val KEY = "Nokia"
    }
     override fun call() {
              // ....
      }
}


class PhoneBeanNotFoundException(p0: String, p1: Throwable?) : RuntimeException(p0, p1)

@SimpleFactoryMethod(beanNotFoundExceptionClass = PhoneBeanNotFoundException::class)
interface PhoneFactory {
    fun get(brand: String): Phone
}



@Component
class CallPhoneBean(private val phoneFactory: PhoneFactory) {

    fun call(brand: String) {
        phoneFactory.get(brand).call()
    }
}

@EnableSimpleFactoryMethod
@SpringBootApplication
class ExampleApplication

fun main(args: Array<String>) {
    SpringApplication.run(ExampleApplication::class.java, *args)
}


```

# Basics Factory method for groups of beans

Usage typically looks like this. 


```kotlin
@SimpleGroupFactoryMethod
interface GroupFactoryMethod {
    fun group(name: String): List<CustomBean>
}


@SimpleGroupFactoryComponent(group = "1")
class CustomBeanAGroup1 : CustomBean

@SimpleGroupFactoryComponent(group = "1")
class CustomBeanBGroup1 : CustomBean

@SimpleGroupFactoryComponent(group = "2")
class CustomBeanCGroup2 : CustomBean


@Autowired
lateinit var groupFactoryMethod: GroupFactoryMethod

val listOfBeansOfGroup1 = groupFactoryMethod.group("1")



```
