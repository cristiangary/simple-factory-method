package com.geektastes

import com.geektastes.config.CustomBean
import com.geektastes.config.ServiceLocatorFactory
import org.springframework.stereotype.Component

@Component
class ProcessCustomBean(private val serviceLocatorFactory: ServiceLocatorFactory) {

    fun customBean(name: String): CustomBean {
        return serviceLocatorFactory.customBean(name)
    }
}
