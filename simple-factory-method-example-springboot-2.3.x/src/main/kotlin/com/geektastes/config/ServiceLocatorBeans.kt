package com.geektastes.config

import com.geektastes.simple.SimpleFactoryMethod
import com.geektastes.simple.SimpleFactoryComponent
import org.springframework.stereotype.Component
import java.io.Serializable


class CustomBeanException(p0: String, p1: Throwable?) : RuntimeException(p0, p1)

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException::class)
interface ServiceLocatorFactory {
    fun customBean(name: String): CustomBean
}

interface CustomBean

@Component
class CustomBeanA : CustomBean

@Component
class CustomBeanB : CustomBean

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "C")
@Component
class CustomBeanC : CustomBean, Serializable, Cloneable

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "D")
@Component
class CustomBeanD : CustomBean, Serializable, Cloneable

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = CustomBeanF.KEY)
@Component
class CustomBeanF : CustomBean {
    companion object {
        const val KEY = "F"
    }
}

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "E")
class CustomBeanE

