package com.geektastes.simple

import org.springframework.beans.FatalBeanException
import org.springframework.beans.factory.BeanFactory
import org.springframework.beans.factory.BeanFactoryAware
import org.springframework.beans.factory.ListableBeanFactory
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition
import org.springframework.beans.factory.config.BeanDefinitionHolder
import org.springframework.beans.factory.support.AbstractBeanDefinition
import org.springframework.beans.factory.support.BeanDefinitionBuilder
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.context.ResourceLoaderAware
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar
import org.springframework.core.io.ResourceLoader
import org.springframework.core.type.AnnotationMetadata
import org.springframework.util.Assert
import org.springframework.util.ClassUtils
import org.springframework.util.StringUtils
import java.util.*

/**
 * This class register the interface annotated with [SimpleFactoryMethod] and create [SimpleFactoryMethodFactoryBean] a bean proxy in spring context
 * this is activated by annotation [EnableSimpleFactoryMethod]
 * @version 1.0.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
class SimpleFactoryMethodRegistrar : ImportBeanDefinitionRegistrar, ResourceLoaderAware, BeanFactoryAware {

    private lateinit var beanFactory: ListableBeanFactory
    private lateinit var resourceLoader: ResourceLoader

    override fun setBeanFactory(beanFactory: BeanFactory) {
        if (beanFactory !is ListableBeanFactory) {
            throw FatalBeanException(
                    "SimpleFactoryMethodRegistrar needs to run in a BeanFactory that is a ListableBeanFactory")
        }
        this.beanFactory = beanFactory
    }

    override fun setResourceLoader(resourceLoader: ResourceLoader) {
        this.resourceLoader = resourceLoader
    }

    override fun registerBeanDefinitions(metadata: AnnotationMetadata, registry: BeanDefinitionRegistry) {
        val basePackages = getBasePackages(metadata)

        registerSimpleFactoryMethod(basePackages, registry)
        registerSimpleGroupFactoryMethod(basePackages, registry)
    }

    private fun registerSimpleGroupFactoryMethod(basePackages: Set<String>, registry: BeanDefinitionRegistry) {
        val scannerFactoryMethodInterface = InterfaceClassPathScanningSimpleGroupFactoryMethodCandidates()
        scannerFactoryMethodInterface.resourceLoader = this.resourceLoader

        for (basePackage in basePackages) {
            val factoryMethodInterface = scannerFactoryMethodInterface.findCandidateComponents(basePackage)
            for (factoryMethod in factoryMethodInterface) {
                if (factoryMethod is AnnotatedBeanDefinition) {

                    val annotationMetadata = factoryMethod.metadata

                    Assert.isTrue(annotationMetadata.isInterface, "@SimpleGroupFactoryMethod can only be specified on an interface")

                    val definition = BeanDefinitionBuilder.genericBeanDefinition(SimpleGroupFactoryMethodFactoryBean::class.java)
                    definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE)
                    val beanDefinition = definition.beanDefinition
                    beanDefinition.isPrimary = true

                    definition.addConstructorArgValue(Class.forName(factoryMethod.beanClassName))

                    val holder = BeanDefinitionHolder(beanDefinition, annotationMetadata.className, arrayOf())
                    BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry)

                }
            }

        }

    }

    private fun registerSimpleFactoryMethod(basePackages: Set<String>, registry: BeanDefinitionRegistry) {

        val scannerFactoryMethodInterface = InterfaceClassPathScanninSimpleFactoryMethodgCandidates()
        scannerFactoryMethodInterface.resourceLoader = this.resourceLoader

        for (basePackage in basePackages) {
            val factoryMethodInterface = scannerFactoryMethodInterface.findCandidateComponents(basePackage)
            for (factoryMethod in factoryMethodInterface) {
                if (factoryMethod is AnnotatedBeanDefinition) {

                    val annotationMetadata = factoryMethod.metadata

                    val attributes = annotationMetadata.getAnnotationAttributes(SimpleFactoryMethod::class.java.canonicalName)

                    val customExceptionClass = attributes["beanNotFoundExceptionClass"]

                    Assert.isTrue(annotationMetadata.isInterface, "@SimpleFactoryMethod can only be specified on an interface")

                    val definition = BeanDefinitionBuilder.genericBeanDefinition(SimpleFactoryMethodFactoryBean::class.java)
                    definition.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE)
                    val beanDefinition = definition.beanDefinition
                    beanDefinition.isPrimary = true

                    definition.addConstructorArgValue(Class.forName(factoryMethod.beanClassName))
                    definition.addConstructorArgValue(customExceptionClass!!)

                    val holder = BeanDefinitionHolder(beanDefinition, annotationMetadata.className,
                            arrayOf())
                    BeanDefinitionReaderUtils.registerBeanDefinition(holder, registry)


                }
            }

        }
    }

    private fun getBasePackages(importingClassMetadata: AnnotationMetadata): Set<String> {
        val attributes = importingClassMetadata.getAnnotationAttributes(EnableSimpleFactoryMethod::class.java.canonicalName)

        val basePackages = HashSet<String>()

        if (attributes["values"] != null) {
            for (pkg in attributes["value"] as Array<*>) {
                if (StringUtils.hasText(pkg as String)) {
                    basePackages.add(pkg)
                }
            }
        }

        if (attributes["basePackages"] != null) {
            for (pkg in attributes["basePackages"] as Array<*>) {
                if (StringUtils.hasText(pkg as String)) {
                    basePackages.add(pkg)
                }
            }
        }

        if (attributes["basePackageClasses"] != null) {
            for (clazz in attributes["basePackageClasses"] as Array<*>) {
                basePackages.add(ClassUtils.getPackageName(clazz as Class<*>))
            }
        }

        if (basePackages.isEmpty()) {
            basePackages.add(ClassUtils.getPackageName(importingClassMetadata.className))
        }
        return basePackages
    }

}
