package com.geektastes.simple

import org.springframework.beans.factory.FactoryBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.core.annotation.AnnotationUtils
import org.springframework.util.ReflectionUtils
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Proxy
import java.util.concurrent.ConcurrentHashMap


class SimpleGroupFactoryMethodFactoryBean(private val simpleGroupFactoryMethodInterface: Class<*>) : FactoryBean<Any>, InitializingBean, ApplicationContextAware {

    private lateinit var proxy: Any
    private lateinit var applicationContext: ApplicationContext

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        this.applicationContext = applicationContext
    }

    override fun afterPropertiesSet() {
        this.proxy = Proxy.newProxyInstance(this.simpleGroupFactoryMethodInterface.classLoader, arrayOf(this.simpleGroupFactoryMethodInterface), ServiceLocatorInvocationHandler())
    }

    override fun isSingleton(): Boolean = true

    override fun getObject(): Any = this.proxy

    override fun getObjectType(): Class<*> = this.simpleGroupFactoryMethodInterface

    private inner class ServiceLocatorInvocationHandler : InvocationHandler {

        private val cache = ConcurrentHashMap<String, Set<Any>>()

        @Throws(Throwable::class)
        override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any {
            return when {
                ReflectionUtils.isEqualsMethod(method) -> proxy === args[0]
                ReflectionUtils.isHashCodeMethod(method) -> System.identityHashCode(proxy)
                ReflectionUtils.isToStringMethod(method) -> "Simple group factory method: $simpleGroupFactoryMethodInterface"
                else -> invokeSimpleGroupFactoryMethod(method, args)
            }
        }

        @Throws(Exception::class)
        private fun invokeSimpleGroupFactoryMethod(method: Method, args: Array<Any>): Any {

            val group = group(args)

            if (group.isNullOrBlank()) {
                throw GroupNotFoundBySimpleGroupFactoryMethodException("Bean groups $args, not found")
            }

            if (cache.containsKey(group)) {
                return cache[group]!!.toList()
            }

            val groupOfBeans = hashSetOf<Any>()

            val annotatedClazzes = applicationContext.getBeansWithAnnotation(SimpleGroupFactoryComponent::class.java)
            for (annotatedClazz in annotatedClazzes) {

                val annotation = AnnotationUtils.findAnnotation(annotatedClazz.value.javaClass, SimpleGroupFactoryComponent::class.java)

                if (annotation.group == group && validateMethodParameterizedTypeListIsSame(method, annotatedClazz.value.javaClass)) {

                    groupOfBeans.add(annotatedClazz.value)

                }
            }

            cache[group] = groupOfBeans

            return groupOfBeans.toList()
        }

        private fun validateMethodParameterizedTypeListIsSame(method: Method, bean: Class<Any>): Boolean {
            val interfaceMethod = simpleGroupFactoryMethodInterface.getMethod(method.name, *(method.parameterTypes))

            val returnType = interfaceMethod.genericReturnType
            if (returnType is ParameterizedType) {
                val typeArguments = returnType.actualTypeArguments
                for (typeArgument in typeArguments) {
                    if ((typeArgument as Class<*>).isAssignableFrom(bean)) {
                        return true
                    }
                }
            }

            return false
        }


        private fun group(args: Array<Any>): String? {
            var beanName: String? = null
            if (args.size == 1) {
                beanName = args[0].toString()
            }
            return beanName
        }

    }


}
