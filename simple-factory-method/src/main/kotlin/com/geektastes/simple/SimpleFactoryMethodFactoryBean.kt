package com.geektastes.simple

import org.springframework.beans.BeanUtils
import org.springframework.beans.BeansException
import org.springframework.beans.factory.FactoryBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.core.annotation.AnnotationUtils
import org.springframework.util.ReflectionUtils
import java.lang.reflect.Constructor
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method
import java.lang.reflect.Proxy
import java.util.concurrent.ConcurrentHashMap


class SimpleFactoryMethodFactoryBean(private val simpleFactoryMethodInterface: Class<*>,
                                     serviceLocatorExceptionClass: Class<out Exception>) : FactoryBean<Any>, InitializingBean, ApplicationContextAware {

    private lateinit var proxy: Any
    private lateinit var applicationContext: ApplicationContext
    private var simpleFactoryMethodExceptionConstructor: Constructor<out Exception>

    init {
        this.simpleFactoryMethodExceptionConstructor = simpleFactoryMethodExceptionConstructor(serviceLocatorExceptionClass)
    }

    override fun setApplicationContext(applicationContext: ApplicationContext) {
        this.applicationContext = applicationContext
    }

    override fun afterPropertiesSet() {
        this.proxy = Proxy.newProxyInstance(this.simpleFactoryMethodInterface.classLoader, arrayOf(this.simpleFactoryMethodInterface), ServiceLocatorInvocationHandler())
    }

    override fun isSingleton(): Boolean = true

    override fun getObject(): Any = this.proxy

    override fun getObjectType(): Class<*> = this.simpleFactoryMethodInterface


    private fun simpleFactoryMethodExceptionConstructor(exceptionClass: Class<out Exception>): Constructor<out Exception> {
        return try {
            exceptionClass.getConstructor(String::class.java, Throwable::class.java) as Constructor<out Exception>
        } catch (ex: NoSuchMethodException) {
            try {
                exceptionClass.getConstructor(Throwable::class.java) as Constructor<out Exception>
            } catch (ex2: NoSuchMethodException) {
                try {
                    exceptionClass.getConstructor(String::class.java) as Constructor<out Exception>
                } catch (ex3: NoSuchMethodException) {
                    throw IllegalArgumentException(
                            "Simple factory method exception [${exceptionClass.name} ] neither has a (String, Throwable) constructor nor a (String) constructor")
                }

            }
        }
    }

    private inner class ServiceLocatorInvocationHandler : InvocationHandler {

        private var cache: ConcurrentHashMap<String, Any> = ConcurrentHashMap()

        @Throws(Throwable::class)
        override fun invoke(proxy: Any, method: Method, args: Array<Any>): Any {
            return when {
                ReflectionUtils.isEqualsMethod(method) -> proxy === args[0]
                ReflectionUtils.isHashCodeMethod(method) -> System.identityHashCode(proxy)
                ReflectionUtils.isToStringMethod(method) -> "Simple factory method: $simpleFactoryMethodInterface"
                else -> invokeSimpleFactoryMethod(method, args)
            }
        }

        @Throws(Exception::class)
        private fun invokeSimpleFactoryMethod(method: Method, args: Array<Any>): Any {

            try {
                val key = key(args)

                if (key.isBlank()) {
                    throw BeanNotFoundBySimpleFactoryMethodException("Bean names $args, not found")
                }

                if (cache.containsKey(key)) {
                    return cache[key]!!
                }

                val annotatedClazzes = applicationContext.getBeansWithAnnotation(SimpleFactoryComponent::class.java)

                for (annotatedClazz in annotatedClazzes) {

                    val annotation = AnnotationUtils.findAnnotation(annotatedClazz.value.javaClass, SimpleFactoryComponent::class.java)

                    if (annotation.factory.java == method.declaringClass && annotation.key == key) {

                        if (validateMethodParameterizedTypeListIsSame(method, annotatedClazz.value.javaClass)) {
                            return annotatedClazz.value
                        }

                    }
                }

                throw BeanNotFoundBySimpleFactoryMethodException("Bean $key, not found")


            } catch (ex: BeansException) {
                throw createCustomSimpleFactoryMethodException(simpleFactoryMethodExceptionConstructor, ex)
            }

        }


        private fun validateMethodParameterizedTypeListIsSame(method: Method, bean: Class<Any>): Boolean {
            val interfaceMethod = simpleFactoryMethodInterface.getMethod(method.name, *(method.parameterTypes))
            return interfaceMethod.returnType.isAssignableFrom(bean)
        }

        private fun createCustomSimpleFactoryMethodException(exceptionConstructor: Constructor<out Exception>, cause: BeansException): Exception {
            val paramTypes = exceptionConstructor.parameterTypes
            val args = arrayOfNulls<Any>(paramTypes.size)
            for (i in paramTypes.indices) {
                if (String::class.java == paramTypes[i]) {
                    args[i] = cause.message
                } else if (paramTypes[i].isInstance(cause)) {
                    args[i] = cause
                }
            }
            return BeanUtils.instantiateClass(exceptionConstructor, *args)
        }


        private fun key(args: Array<Any>): String {
            if (args.size == 1) {
                return args[0].toString()
            }
            return ""
        }

        @Throws(NoSuchMethodException::class)
        private fun getSimpleFactoryMethodReturnType(method: Method): Class<*> {
            val interfaceMethod = simpleFactoryMethodInterface.getMethod(method.name, *(method.parameterTypes))

            if (method.parameterTypes.size > 1 || Void.TYPE == interfaceMethod.returnType) {
                throw UnsupportedOperationException(
                        "May only call methods with signature '<type> xxx()' or '<type> xxx(<idtype> id)' " +
                                "on factory interface, but tried to call: " + interfaceMethod)
            }
            return interfaceMethod.returnType
        }
    }

}
