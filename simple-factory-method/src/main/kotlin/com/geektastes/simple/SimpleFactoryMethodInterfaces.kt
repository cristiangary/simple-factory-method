package com.geektastes.simple

import org.springframework.beans.BeansException
import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.Import
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.stereotype.Component
import kotlin.reflect.KClass

/**
 * beans annotated with this will be associated to be used with the interface annotated with [SimpleFactoryMethod]
 * associated to specific `key`
 * @version 1.0.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class SimpleFactoryComponent(

        /**
         * Interface annotated with <code>SimpleFactoryMethod</code>, used to map the bean with [.key]
         */
        val factory: KClass<*>,

        /**
         * The <code>key</code> used by the interface that are annotated with [SimpleFactoryMethod]
         * this <code>key</code> is used to get the bean from factory method declared in [.factory]
         */
        val key: String

)



/**
 * beans annotated with this will be associated to be used with the interface annotated with [SimpleGroupFactoryMethod]
 * and associated to specific `group`
 * @version 1.1.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class SimpleGroupFactoryComponent(

        /**
         * The <code>group</code> used by the interface that are annotated with [SimpleGroupFactoryMethod]
         * this <code>group</code> is used to get all the beans in this group
         */
        val group: String = "")

/**
 * Annotation for interface that are used to create factory method. That interface can be autowired and used in spring context
 * @throws [BeanNotFoundBySimpleFactoryMethodException] or custom exception declared in [beanNotFoundExceptionClass]
 * when the bean trying to get is not found.
 * @version 1.0.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@Component
annotation class SimpleFactoryMethod(
        /**
         * The exception used to customize the throw in case the bean are not found. If this value is not setter the default [BeanNotFoundBySimpleFactoryMethodException]
         * are used
         */
        val beanNotFoundExceptionClass: KClass<*> = BeanNotFoundBySimpleFactoryMethodException::class
)

/**
 * Annotation for interface that are used to create factory method. That interface can be autowired and used in spring context
 * @throws [GroupNotFoundBySimpleGroupFactoryMethodException]
 * when the interface don't have method with the group as parameter.
 * @version 1.1.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@Component
annotation class SimpleGroupFactoryMethod


/**
 * This Annotation enable the scan of interface annotated with [SimpleFactoryMethod], so can be autowired in spring context.
 * @version 1.0.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.TYPE, AnnotationTarget.CLASS)
@Import(SimpleFactoryMethodRegistrar::class)
annotation class EnableSimpleFactoryMethod

/**
 * Default exception when the interface annotated with [SimpleFactoryMethod] can't find the requested bean
 * @version 1.0.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
class BeanNotFoundBySimpleFactoryMethodException(msg: String) : BeansException(msg)

/**
 * Default exception when the interface annotated with [SimpleFactoryMethod] dont have group parameter
 * @version 1.1.0
 * @author Cristian Gary-Bufadel cristiangary@gmail.com
 */
class GroupNotFoundBySimpleGroupFactoryMethodException(msg: String) : BeansException(msg)

class InterfaceClassPathScanninSimpleFactoryMethodgCandidates(useDefaultFilter: Boolean = false) : ClassPathScanningCandidateComponentProvider(useDefaultFilter) {
    init {
        addIncludeFilter(AnnotationTypeFilter(SimpleFactoryMethod::class.java, false))
    }

    override fun isCandidateComponent(beanDefinition: AnnotatedBeanDefinition): Boolean = beanDefinition.metadata.isInterface
}

class InterfaceClassPathScanningSimpleGroupFactoryMethodCandidates(useDefaultFilter: Boolean = false) : ClassPathScanningCandidateComponentProvider(useDefaultFilter) {
    init {
        addIncludeFilter(AnnotationTypeFilter(SimpleGroupFactoryMethod::class.java, false))
    }

    override fun isCandidateComponent(beanDefinition: AnnotatedBeanDefinition): Boolean = beanDefinition.metadata.isInterface
}
