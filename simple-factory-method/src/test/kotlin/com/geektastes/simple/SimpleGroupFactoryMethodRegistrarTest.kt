package com.geektastes.simple

import junit.framework.TestCase.assertTrue
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.io.Serializable


@SimpleGroupFactoryComponent(group = "1")
class CustomBeanAGroup1 : CustomBean

@SimpleGroupFactoryComponent(group = "1")
class CustomBeanBGroup1 : CustomBean

@SimpleGroupFactoryComponent(group = "2")
class CustomBeanCGroup2 : CustomBean, Serializable, Cloneable

@SimpleGroupFactoryComponent(group = "2")
class CustomBeanDGroup2 : CustomBean, Serializable, Cloneable

@SimpleGroupFactoryComponent(group = CustomBeanFGroup2.GROUP)
class CustomBeanFGroup2 : CustomBean {
    companion object {
        const val GROUP = "2"
    }
}

open class CustomSuperBean

open class CustomSuperBean2:CustomSuperBean()

open class CustomSuperBean3:CustomSuperBean2()

@SimpleGroupFactoryComponent(group = "1")
class CustomBeanHGroup1 : CustomSuperBean2()

@SimpleGroupFactoryComponent(group = "1")
class CustomBeanIGroup1 : CustomSuperBean2()

@SimpleGroupFactoryComponent(group = "1")
class CustomBeanJGroup1 : CustomSuperBean3()

@SimpleGroupFactoryMethod
interface GroupFactoryMethod {
    fun group(name: String): List<CustomBean>
}

@SimpleGroupFactoryMethod
interface GroupSuperClassFactoryMethod {
    fun group(name: String): List<CustomSuperBean>
}

@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
class SimpleGroupFactoryMethodRegistrarTest {


    @Configuration
    @EnableSimpleFactoryMethod
    internal class Config {
        @Bean
        fun customBeanFGroup2(): CustomBean {
            return CustomBeanFGroup2()
        }

        @Bean
        fun customBeanHGroup1(): CustomSuperBean {
            return CustomBeanHGroup1()
        }

        @Bean
        fun customBeanIGroup1(): CustomSuperBean {
            return CustomBeanIGroup1()
        }

        @Bean
        fun customBeanJGroup1(): CustomSuperBean {
            return CustomBeanJGroup1()
        }

        @Bean
        fun customBeanDGroup2(): CustomBean {
            return CustomBeanDGroup2()
        }

        @Bean("OTHER_BEAN_NAME")
        fun customBeanBGroup1(): CustomBean {
            return CustomBeanBGroup1()
        }

        @Bean
        fun customBeanAGroup1(): CustomBean {
            return CustomBeanAGroup1()
        }

        @Bean("ANOTHER_BEAN_NAME")
        fun customBeanCGroup2(): CustomBean {
            return CustomBeanCGroup2()
        }
    }


    @Autowired
    lateinit var groupFactoryMethod: GroupFactoryMethod

    @Autowired
    lateinit var groupSuperClassFactoryMethod: GroupSuperClassFactoryMethod


    @Test
    fun `given the group 1 when we request to groupSuperClassFactoryMethod get that group then should get the list of bean that extend CustomSuperBean and is part of the group 1`() {
        val beans = groupSuperClassFactoryMethod.group("1")
        Assert.assertEquals(3, beans.size)

        for (bean in beans) {
            assertTrue(bean is CustomBeanHGroup1 || bean is CustomBeanIGroup1 || bean is CustomBeanJGroup1)
        }
    }

    @Test
    fun `given the group 1 when we request to groupFactoryMethod get that group then should get the list of bean CustomBeanBGroup1 and CustomBeanAGroup1`() {

        val beans = groupFactoryMethod.group("1")
        Assert.assertEquals(2, beans.size)

        for (bean in beans) {
            assertTrue(bean is CustomBeanBGroup1 || bean is CustomBeanAGroup1)
        }
    }

    @Test
    fun `given the group 2 when we request to groupFactoryMethod get that group then should get the list of bean CustomBeanFGroup2 and CustomBeanDGroup2`() {

        val beans = groupFactoryMethod.group("2")
        Assert.assertEquals(3, beans.size)

        for (bean in beans) {
            assertTrue(bean is CustomBeanFGroup2 || bean is CustomBeanDGroup2 || bean is CustomBeanCGroup2)
        }
    }
}
