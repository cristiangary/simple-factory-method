package com.geektastes.simple

import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import java.io.Serializable


class CustomBeanException(p0: String, p1: Throwable?) : RuntimeException(p0, p1)

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException::class)
interface ServiceLocatorFactory {
    fun customBean(name: String): CustomBean
}

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException::class)
interface ServiceLocatorFactoryForBeanA {
    fun get(name: String): CustomBean
}

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException::class)
interface ServiceLocatorFactoryForBeanB {
    fun get(type: Type): CustomBean
}

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException::class)
interface ServiceLocatorFactoryForExtendedClass {
    fun get(name: String): CustomSuperBean10
}

interface CustomBean

@SimpleFactoryComponent(factory = ServiceLocatorFactoryForBeanA::class, key = "A")
class CustomBeanA : CustomBean


@SimpleFactoryComponent(factory = ServiceLocatorFactoryForBeanB::class, key = "B")
class CustomBeanB : CustomBean

enum class Type{
    B
}

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "C")
class CustomBeanC : CustomBean, Serializable, Cloneable

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "D")
class CustomBeanD : CustomBean, Serializable, Cloneable

@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = CustomBeanF.KEY)
class CustomBeanF : CustomBean {
    companion object {
        const val KEY = "F"
    }
}

open class CustomSuperBean10 : CustomBean
open class CustomSuperBean11 : CustomSuperBean10()

open class CustomSuperBean12 : CustomSuperBean10()


@SimpleFactoryComponent(factory = ServiceLocatorFactory::class, key = "12")
class CustomBean12 : CustomSuperBean12()


@SimpleFactoryComponent(factory = ServiceLocatorFactoryForExtendedClass::class, key = "11")
class CustomBean11 : CustomSuperBean11()


@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration
class SimpleLocatorFactoryRegistrarTest {

    @Configuration
    @EnableSimpleFactoryMethod
    internal class Config {

        @Bean
        fun customBeanF(): CustomBean {
            return CustomBeanF()
        }

        @Bean
        fun customBeanD(): CustomBean {
            return CustomBeanD()
        }

        @Bean
        fun customBeanA(): CustomBean {
            return CustomBeanA()
        }

        @Bean
        fun customBean11(): CustomSuperBean10 {
            return CustomBean11()
        }

        @Bean
        fun customBean12(): CustomBean {
            return CustomBean12()
        }

        @Bean("ANOTHER_BEAN_NAME")
        fun customBeanC(): CustomBean {
            return CustomBeanC()
        }

        @Bean
        fun customBeanB(): CustomBean {
            return CustomBeanB()
        }


    }

    @Autowired
    lateinit var serviceLocatorFactory: ServiceLocatorFactory

    @Autowired
    lateinit var serviceLocatorFactoryForBeanA: ServiceLocatorFactoryForBeanA

    @Autowired
    lateinit var serviceLocatorFactoryForExtendedClass: ServiceLocatorFactoryForExtendedClass

    @Autowired
    lateinit var serviceLocatorFactoryForBeanB: ServiceLocatorFactoryForBeanB

    @Test(expected = CustomBeanException::class)
    fun `given the name of the bean not annotated with @SimpleFactoryComponent when we call the servicelocator then we should get the exception`() {

        serviceLocatorFactory.customBean("A")
        serviceLocatorFactory.customBean("B")
        serviceLocatorFactory.customBean("ASDADS")
    }


    @Test(expected = CustomBeanException::class)
    fun `given the name of the bean is annotated with @SimpleFactoryComponent when we call the servicelocator then we should get the exception even the name of bean is good `() {
        serviceLocatorFactory.customBean("ANOTHER_BEAN_NAME")
    }


    @Test(expected = CustomBeanException::class)
    fun `given the name of the bean is annotated with @SimpleFactoryComponent when we call the servicelocator then we should get the exception`() {
        serviceLocatorFactory.customBean("A")
    }

    @Test(expected = CustomBeanException::class)
    fun `given i have bean annotated with @SimpleFactoryComponent and key E but don't implement interface of the servicelocator when i call the servicelocator with the key E then we should get the bean annotated with Key C`() {
        serviceLocatorFactory.customBean("E")
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key D when i call the servicelocator with the key D then we should get the bean annotated with Key D`() {
        assertTrue(serviceLocatorFactory.customBean("D") is CustomBeanD)
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key C when i call the servicelocator with the key C then we should get the bean annotated with Key C`() {
        assertTrue(serviceLocatorFactory.customBean("C") is CustomBeanC)
    }


    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key F when i call the servicelocator with the key F then we should get the bean annotated with Key F`() {
        assertTrue(serviceLocatorFactory.customBean("F") is CustomBeanF)
        assertTrue(serviceLocatorFactory.customBean("F") is CustomBeanF)
        assertTrue(serviceLocatorFactory.customBean("F") is CustomBeanF)
        assertTrue(serviceLocatorFactory.customBean("F") is CustomBeanF)
        assertTrue(serviceLocatorFactory.customBean("F") is CustomBeanF)
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key F when i call the servicelocator with the key A then we should get the bean annotated with Key A`() {
        assertTrue(serviceLocatorFactoryForBeanA.get("A") is CustomBeanA)
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key 11 when i call the servicelocator with the key 11 then we should get the bean annotated with Key 11`() {
        assertTrue(serviceLocatorFactoryForExtendedClass.get("11") is CustomBean11)
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key 12 when i call the servicelocator with the key 12 then we should get the bean annotated with Key 12`() {
        assertTrue(serviceLocatorFactory.customBean("12") is CustomBean12)
    }

    @Test
    fun `given i have bean annotated with @SimpleFactoryComponent and key B when i call the servicelocator with the key ENUM Type B then we should get the bean annotated with Key B`() {
        assertTrue(serviceLocatorFactoryForBeanB.get(Type.B) is CustomBeanB)
    }
}
