package com.geektastes

import com.geektastes.config.CustomBean
import com.geektastes.config.ServiceLocatorFactory
import com.geektastes.config.SimpleGroupFactory
import org.springframework.stereotype.Component

@Component
class ProcessCustomBean(private val serviceLocatorFactory: ServiceLocatorFactory, private val simpleGroupFactory: SimpleGroupFactory) {

    fun customBean(name: String): CustomBean {
        return serviceLocatorFactory.customBean(name)
    }

    fun group(id:String):List<CustomBean>{
        return simpleGroupFactory.group(id)
    }
}
