package com.geektastes

import com.geektastes.simple.EnableSimpleFactoryMethod
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@EnableSimpleFactoryMethod
@SpringBootApplication
class ServiceLocatorApplication

fun main(args: Array<String>) {
    SpringApplication.run(ServiceLocatorApplication::class.java, *args)
}
