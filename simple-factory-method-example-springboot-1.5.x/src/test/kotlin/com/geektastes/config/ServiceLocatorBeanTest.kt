package com.geektastes.config

import com.geektastes.ProcessCustomBean
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class ServiceLocatorBeanTest {

    @Autowired
    lateinit var processCustomBean: ProcessCustomBean

    @Test(expected = CustomBeanException::class)
    fun `given the name of the bean not annotated with @ServiceLocator when we call the servicelocator then we should get the exception`() {

        processCustomBean.customBean("A")
        processCustomBean.customBean("B")
        processCustomBean.customBean("ASDADS")
    }


    @Test(expected = CustomBeanException::class)
    fun `given the name of the bean is annotated with @ServiceLocator when we call the servicelocator then we should get the exception`() {
        assertTrue(processCustomBean.customBean("A") is CustomBeanA)
    }


    @Test(expected = CustomBeanException::class)
    fun `given i have bean annotated with @ServiceLocator and key E but don't implement interface of the servicelocator when i call the servicelocator with the key E then we should get the bean annotated with Key C`() {
        processCustomBean.customBean("E")
    }

    @Test
    fun `given i have bean annotated with @ServiceLocator and key D when i call the servicelocator with the key D then we should get the bean annotated with Key D`() {
        assertTrue(processCustomBean.customBean("D") is CustomBeanD)
    }

    @Test
    fun `given i have bean annotated with @ServiceLocator and key C when i call the servicelocator with the key C then we should get the bean annotated with Key C`() {
        assertTrue(processCustomBean.customBean("C") is CustomBeanC)
    }


    @Test
    fun `given i have bean annotated with @ServiceLocator and key F when i call the servicelocator with the key F then we should get the bean annotated with Key F`() {
        assertTrue(processCustomBean.customBean("F") is CustomBeanF)
    }

    @Test
    fun `given group id 1 when i call simpleGroupFactory then i should get one bean`() {
        assertEquals(1, processCustomBean.group("1").size)
        assertTrue(processCustomBean.group("1")[0] is CustomBeanC)
    }

    @Test
    fun `given group id 2 when i call simpleGroupFactory then i should get two bean`() {
        assertEquals(2, processCustomBean.group("2").size)
        for (customBean in processCustomBean.group("2")) {
            assertTrue(customBean is CustomBeanD || customBean is CustomBeanF)
        }
    }


}
