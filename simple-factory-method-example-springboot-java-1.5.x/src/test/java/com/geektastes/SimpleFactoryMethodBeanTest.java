package com.geektastes;

import static junit.framework.TestCase.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.geektastes.beans.CustomBeanA;
import com.geektastes.beans.CustomBeanC;
import com.geektastes.beans.CustomBeanD;
import com.geektastes.beans.CustomBeanException;
import com.geektastes.beans.CustomBeanF;
import com.geektastes.beans.SimpleFactoryMethodFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleFactoryMethodBeanTest {

    @Autowired
    private SimpleFactoryMethodFactory simpleFactoryMethodFactory;

    @Test(expected = CustomBeanException.class)
    public void givenTheNameOfthBeanNotAnnotatedWithServiceLocatorWhenWeCallTheServicelocatorThenWeShouldGetTheException() {

        simpleFactoryMethodFactory.customBean("A");
        simpleFactoryMethodFactory.customBean("B");
        simpleFactoryMethodFactory.customBean("ASDADS");
    }

    @Test(expected = CustomBeanException.class)
    public void givenTheNameofTheBeanIsAnnotatedWithServiceLocatorWhenWeCalltheServicelocatorThenWeShouldGetTheException() {
        assertTrue(simpleFactoryMethodFactory.customBean("A") instanceof CustomBeanA);
    }

    @Test(expected = CustomBeanException.class)
    public void exceptionWhenBeanDontImplementFactoryInterface() {
        simpleFactoryMethodFactory.customBean("E");
    }

    @Test
    public void shouldGetTheBeanD() {
        assertTrue(simpleFactoryMethodFactory.customBean("D") instanceof CustomBeanD);
    }

    @Test
    public void shouldGetTheBeanC() {
        assertTrue(simpleFactoryMethodFactory.customBean("C") instanceof CustomBeanC);
    }

    @Test
    public void shouldGetTheBeanF() {
        assertTrue(simpleFactoryMethodFactory.customBean("F") instanceof CustomBeanF);
    }
}
