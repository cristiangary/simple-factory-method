package com.geektastes;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.geektastes.beans.CustomBean;
import com.geektastes.beans.CustomBeanA;
import com.geektastes.beans.CustomBeanB;
import com.geektastes.beans.CustomBeanC;
import com.geektastes.beans.CustomBeanD;
import com.geektastes.beans.CustomBeanF;
import com.geektastes.beans.SimpleGroupFactoryMethodFactory;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleGroupFactoryMethodTest {

    @Autowired
    private SimpleGroupFactoryMethodFactory simpleGroupFactoryMethodFactory;

    @Test
    public void shouldGetEmptyGroup() {
        List<CustomBean> groups = simpleGroupFactoryMethodFactory.group("grupoDontExist");
        assertTrue(groups.isEmpty());
    }

    @Test
    public void shouldGetGroup1WithoutBeanG() {
        List<CustomBean> group1 = simpleGroupFactoryMethodFactory.group("group1");

        assertEquals(2, group1.size());

        for (CustomBean customBean : group1) {
            assertTrue(customBean instanceof CustomBeanD || customBean instanceof CustomBeanF);
        }
    }

    @Test
    public void shouldGetGroup2() {
        List<CustomBean> group2 = simpleGroupFactoryMethodFactory.group("group2");

        assertEquals(3, group2.size());

        for (CustomBean customBean : group2) {
            assertTrue(customBean instanceof CustomBeanA || customBean instanceof CustomBeanB
                    || customBean instanceof CustomBeanC);
        }
    }
}
