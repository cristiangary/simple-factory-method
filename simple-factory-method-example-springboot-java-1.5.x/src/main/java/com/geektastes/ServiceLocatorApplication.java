package com.geektastes;

import com.geektastes.beans.All;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.geektastes.beans.Test;
import com.geektastes.simple.EnableSimpleFactoryMethod;

@Test(clazz = All.ALL)
@EnableSimpleFactoryMethod
@SpringBootApplication
public class ServiceLocatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceLocatorApplication.class, args);
    }
}
