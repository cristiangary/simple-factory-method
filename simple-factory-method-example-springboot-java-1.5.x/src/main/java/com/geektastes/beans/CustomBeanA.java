package com.geektastes.beans;

import com.geektastes.simple.SimpleGroupFactoryComponent;
import org.springframework.stereotype.Component;

@SimpleGroupFactoryComponent(group = "group2")
@Component
public class CustomBeanA implements CustomBean{

}
