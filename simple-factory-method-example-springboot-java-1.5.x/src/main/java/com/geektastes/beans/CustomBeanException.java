package com.geektastes.beans;

public class CustomBeanException extends RuntimeException {

    public CustomBeanException() {
    }

    public CustomBeanException(String s) {
        super(s);
    }

    public CustomBeanException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
