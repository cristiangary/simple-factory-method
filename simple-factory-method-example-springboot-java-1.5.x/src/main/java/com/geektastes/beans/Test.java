package com.geektastes.beans;

public @interface Test {
    All clazz() default All.ALL;
}
