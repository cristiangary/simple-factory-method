package com.geektastes.beans;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.geektastes.simple.SimpleGroupFactoryComponent;

@ConditionalOnProperty(value = "custom.bean.g.enabled", havingValue = "true")
@SimpleGroupFactoryComponent(group = "group1")
@Component
public class CustomBeanG implements CustomBean {

}
