package com.geektastes.beans;

import com.geektastes.simple.SimpleFactoryComponent;
import com.geektastes.simple.SimpleGroupFactoryComponent;
import org.springframework.stereotype.Component;

@SimpleGroupFactoryComponent(group = "group2")
@SimpleFactoryComponent(factory = SimpleFactoryMethodFactory.class, key = "C")
@Component
public class CustomBeanC implements CustomBean{

}
