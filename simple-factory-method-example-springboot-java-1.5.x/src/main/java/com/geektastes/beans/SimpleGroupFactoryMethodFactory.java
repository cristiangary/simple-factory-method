package com.geektastes.beans;

import java.util.List;

import com.geektastes.simple.SimpleGroupFactoryMethod;

@SimpleGroupFactoryMethod
public interface SimpleGroupFactoryMethodFactory {

    List<CustomBean> group(String group);
}
