package com.geektastes.beans;

import com.geektastes.simple.SimpleFactoryMethod;

@SimpleFactoryMethod(beanNotFoundExceptionClass = CustomBeanException.class)
public interface SimpleFactoryMethodFactory {
    CustomBean customBean(String name);
}
