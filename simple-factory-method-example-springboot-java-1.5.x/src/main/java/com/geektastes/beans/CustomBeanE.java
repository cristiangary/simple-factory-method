package com.geektastes.beans;

import com.geektastes.simple.SimpleGroupFactoryComponent;
import org.springframework.stereotype.Component;

import com.geektastes.simple.SimpleFactoryComponent;

@SimpleGroupFactoryComponent(group = "group1")
@SimpleFactoryComponent(factory = SimpleFactoryMethodFactory.class, key = "E")
@Component
public class CustomBeanE {

}
