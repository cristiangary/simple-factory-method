package com.geektastes.beans;

import com.geektastes.simple.SimpleFactoryComponent;
import com.geektastes.simple.SimpleGroupFactoryComponent;
import org.springframework.stereotype.Component;

@SimpleGroupFactoryComponent(group = "group1")
@SimpleFactoryComponent(factory = SimpleFactoryMethodFactory.class, key = CustomBeanF.KEY)
@Component
public class CustomBeanF implements CustomBean{
    static final String KEY = "F";
}
